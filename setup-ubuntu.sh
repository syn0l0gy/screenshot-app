#! /bin/bash

### Install nodejs  https://github.com/nodesource/distributions
sudo apt-get install -y ca-certificates curl gnupg
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
NODE_MAJOR=20
echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list
sudo apt-get update
sudo apt-get install nodejs -y
sudo apt-get install -y libglib2.0-0 libnss3 libatk1.0-0 libatk-bridge2.0-0 libcups2 libgbm1 libasound2 libpangocairo-1.0-0 libxss1 libgtk-3-0
### Create screenshot project
sudo curl https://gitlab.com/syn0l0gy/screenshot-app/-/archive/main/screenshot-app-main.tar -o /home/ubuntu/screenshot-app-main.tar
sudo tar -xvf screenshot-app-main.tar -C .
cd screenshot-app-main
sudo npm install
sudo cp /home/ubuntu/screenshot-app-main/screenshot.service /etc/systemd/system/screenshot.service
sudo systemctl enable screenshot.service
sudo systemctl start screenshot.service
rm -f /home/ubuntu/screenshot-app-main.tar
