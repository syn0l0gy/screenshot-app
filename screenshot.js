//- screenshot.js

const puppeteer = require("puppeteer-extra");
const StealthPlugin = require("puppeteer-extra-plugin-stealth");
puppeteer.use(StealthPlugin());

module.exports = {
    getImageBuffer: async (url) => {
        const browser = await puppeteer.launch({
            headless: 'new',
            defaultViewport: null,
            args: [
                "--no-sandbox",
                "--disable-gpu",
                "--window-size=1920,1080",
            ]
        })
    const page = await browser.newPage();
    try {
      await page.goto(url, { waitUntil: "networkidle0", timeout: 10000 });
      const buffer = await page.screenshot({ type: "png", fullPage: true });
      return buffer;
    } catch (e) {
      console.log("Oops! - " + e.message);
      return buffer;
    } finally {
      await page.close();
      await browser.close();
    }
  }
};
