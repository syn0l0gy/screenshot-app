//- index.js

const proxy = require("./proxy.js");
const express = require("express");
const app = express();
const PORT = 80;

app.use("/*", proxy);
app.listen(PORT, () => console.log(`Server started on port ${PORT}`));
