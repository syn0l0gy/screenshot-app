//- proxy.js

const screenshot = require("./screenshot.js");
const express = require("express");
const router = express.Router();

router.get("/*", (req, res) => {
  const fullUrl = req.originalUrl;
  if (fullUrl.match(/^\/http*.:\/\//i)) {
    (async () => {
      const url = fullUrl.substring(1, fullUrl.length);
      const buffer = await screenshot.getImageBuffer(url);
      res.setHeader("content-type", "image/png");
      res.write(buffer, "binary");
      res.end(null, "binary");
      const datetime = new Date();
      console.log(datetime.toLocaleString('en-US'));
      console.log(url);
      const ipAddress = req.header('x-forwarded-for') || req.socket.remoteAddress;
      console.log(ipAddress);
    })();
  } else {
    res.status(204);
    res.end();
  }
});

module.exports = router;
