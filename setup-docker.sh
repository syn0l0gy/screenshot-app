#! /bin/bash

### Install docker https://www.cloudbooklet.com/how-to-install-docker-on-ubuntu-22-04/
sudo apt update
sudo apt upgrade -y
sudo apt install apt-transport-https ca-certificates curl software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt update
sudo apt-cache policy docker-ce
sudo apt install docker-ce -y
sudo usermod -aG docker $USER

### Install Nginx Proxy Manager
sudo docker run -d \
    --name=npm \
    -p 8181:8181 \
    -p 80:8080 \
    -p 443:4443 \
    -v /docker/config/npm:/config:rw \
    jlesage/nginx-proxy-manager:latest

### Install nodejs  https://github.com/nodesource/distributions
sudo apt-get install -y ca-certificates curl gnupg
sudo mkdir -p /etc/apt/keyrings
curl -fsSL https://deb.nodesource.com/gpgkey/nodesource-repo.gpg.key | sudo gpg --dearmor -o /etc/apt/keyrings/nodesource.gpg
NODE_MAJOR=20
echo "deb [signed-by=/etc/apt/keyrings/nodesource.gpg] https://deb.nodesource.com/node_$NODE_MAJOR.x nodistro main" | sudo tee /etc/apt/sources.list.d/nodesource.list
sudo apt-get update
sudo apt-get install nodejs -y

### Create screenshot docker image
sudo curl https://gitlab.com/syn0l0gy/screenshot-app/-/archive/main/screenshot-app-main.tar -o /home/ubuntu/screenshot-app-main.tar
sudo tar -xvf screenshot-app-main.tar -C .
cd screenshot-app-main
sudo docker build -t headless-chrome .
sudo docker run -d --init --name headless-chrome --restart unless-stopped -p 4000:80 headless-chrome

### Create Docker custom bridge network
sudo docker network create -d bridge custom_bridge
sudo docker network connect custom_bridge npm
sudo docker network connect custom_bridge headless-chrome

### Configure lazydocker
if ! grep -q lazydocker /home/ubuntu/.bashrc; then
  echo "alias lazydocker='sudo docker run --rm -it -v /var/run/docker.sock:/var/run/docker.sock -v /docker/config/lazydocker:/.config/jesseduffield/lazydocker lazyteam/lazydocker'" >> /home/ubuntu/.bashrc
fi
